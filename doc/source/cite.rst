Citing SAILS
=================================
|

If this package has contributed to your work, please include the following citations:

.. title image, description
.. raw:: html

    <div class="container" style="margin-bottom:10px">
        Quinn, A. J., & Hymers, M (2020). SAILS: Spectral Analysis In Linear Systems. Journal of Open Source Software, 5(47), 1982,<br>
        <a href=https://doi.org/10.21105/joss.01982>https://doi.org/10.21105/joss.01982</a>
    </div>

    <div class="container" style="margin-bottom:10px">
    Quinn, A. J., Green, G. G. R., & Hymers, M. (2021). Delineating between-subject heterogeneity in alpha networks with Spatio-Spectral Eigenmodes. NeuroImage, 118330.<br><a href=https://doi.org/10.1016/j.neuroimage.2021.118330>https://doi.org/10.1016/j.neuroimage.2021.118330</a>
    </div>
|



.. highlight:: none
::

    @article{Quinn2020,
      doi = {10.21105/joss.01982},
      url = {https://doi.org/10.21105/joss.01982},
      year = {2020},
      publisher = {The Open Journal},
      volume = {5},
      number = {47},
      pages = {1982},
      author = {Andrew J. Quinn and Mark Hymers},
      title = {SAILS: Spectral Analysis In Linear Systems},
      journal = {Journal of Open Source Software}
    }

    @article{Quinn2021,
      doi = {10.1016/j.neuroimage.2021.118330},
      url = {https://doi.org/10.1016/j.neuroimage.2021.118330},
      year = {2021},
      month = jul,
      publisher = {Elsevier {BV}},
      pages = {118330},
      author = {Andrew J. Quinn and Gary G.R. Green and Mark Hymers},
      title = {Delineating between-subject heterogeneity in alpha networks with Spatio-Spectral Eigenmodes},
      journal = {{NeuroImage}}
    }
