Model Fitting
=============

Model fitting is normally performed by either the
:class:`sails.modelfit.VieiraMorfLinearModel` or
:class:`sails.modelfit.OLSLinearModel` class.  If you wish
to fit a model using another method, an example of this
can be found in :ref:tutorial8.

.. autoclass:: sails.modelfit.VieiraMorfLinearModel
   :members:

.. autoclass:: sails.modelfit.OLSLinearModel
   :members:


Model fitting helper functions
##############################

In addition to basic model fitting routines, some helper
functions exist; for instance to help with fitting a number of
models to a data series following a "sliding window" pattern
or by applying PCA before the fit.

.. autofunction:: sails.modelfit.sliding_window_fit
.. autofunction:: sails.modelfit.pca_reduced_fit
