Support routines
################

Abstract Classes
****************

.. autoclass:: sails.modelfit.AbstractLinearModel
   :members:

.. autoclass:: sails.mvar_metrics.AbstractMVARMetrics
   :members:

Statistical Functions
*********************

.. automodule:: sails.stats
   :members:

.. automodule:: sails.modelvalidation
   :members:

Data Processing Functions
*************************

.. autofunction:: sails.modal.adjust_phase

.. autofunction:: sails.orthogonalise.symmetric_orthonormal
.. autofunction:: sails.orthogonalise.innovations

Tutorial helper functions
*************************

.. automodule:: sails.tutorial_utils
   :members:


