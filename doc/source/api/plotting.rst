Plotting
########

.. autofunction:: sails.plotting.root_plot
.. autofunction:: sails.plotting.plot_vector
.. autofunction:: sails.plotting.plot_matrix

.. autoclass:: sails.circular_plots.CircosHandler
.. autoclass:: sails.circular_plots.CircosGroup
