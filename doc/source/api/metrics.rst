Model metrics
=============

.. autoclass:: sails.mvar_metrics.FourierMvarMetrics
   :members:

.. autoclass:: sails.mvar_metrics.ModalMvarMetrics
   :members:

.. autofunction:: sails.mvar_metrics.modal_transfer_function


Metric Mathemetical Functions
#############################

.. autofunction:: sails.mvar_metrics.sdf_spectrum
.. autofunction:: sails.mvar_metrics.psd_spectrum
.. autofunction:: sails.mvar_metrics.ar_spectrum
.. autofunction:: sails.mvar_metrics.transfer_function
.. autofunction:: sails.mvar_metrics.spectral_matrix
.. autofunction:: sails.mvar_metrics.coherency
.. autofunction:: sails.mvar_metrics.partial_coherence
.. autofunction:: sails.mvar_metrics.partial_directed_coherence
.. autofunction:: sails.mvar_metrics.isolated_effective_coherence
.. autofunction:: sails.mvar_metrics.directed_transfer_function
.. autofunction:: sails.mvar_metrics.geweke_granger_causality

