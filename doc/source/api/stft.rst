Short-Time Fourier Transforms
#############################

.. autofunction:: sails.stft.periodogram
.. autofunction:: sails.stft.sw_periodogram
.. autofunction:: sails.stft.multitaper
.. autofunction:: sails.stft.sw_multitaper
.. autofunction:: sails.stft.glm_periodogram
