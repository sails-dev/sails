#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

import sails

plt.style.use('ggplot')

# Baccala 2001 Figure 1

sample_rate = 128

sig_bac2001_fig1 = sails.simulate.Baccala2001_fig1()
m = sig_bac2001_fig1.generate_model()
freq_vect = np.linspace(0, sample_rate / 2, 36)
F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)

pdc_fig = sails.plotting.plot_vector(np.abs(F.partial_directed_coherence),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 1 PDC',
                                     font_size=8)

dtf_fig = sails.plotting.plot_vector(np.abs(F.directed_transfer_function),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 1 DTF',
                                     font_size=8)

pdc_fig.savefig('gallery_bac2001_fig1_pdc.png', dpi=150)
dtf_fig.savefig('gallery_bac2001_fig1_dtf.png', dpi=150)

plt.close('all')

# Baccala 2001 Figure 2

sample_rate = 128

sig_bac2001_fig2 = sails.simulate.Baccala2001_fig2()
m = sig_bac2001_fig2.generate_model()
freq_vect = np.linspace(0, sample_rate / 2, 36)
F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)

pdc_fig = sails.plotting.plot_vector(np.abs(F.partial_directed_coherence),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 2 PDC',
                                     font_size=8)

dtf_fig = sails.plotting.plot_vector(np.abs(F.directed_transfer_function),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 2 DTF',
                                     font_size=8)

pdc_fig.savefig('gallery_bac2001_fig2_pdc.png', dpi=150)
dtf_fig.savefig('gallery_bac2001_fig2_dtf.png', dpi=150)

plt.close('all')

# Baccala 2001 Figure 3

sample_rate = 128

sig_bac2001_fig3 = sails.simulate.Baccala2001_fig3()
m = sig_bac2001_fig3.generate_model()
freq_vect = np.linspace(0, sample_rate / 2, 36)
F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)

pdc_fig = sails.plotting.plot_vector(np.abs(F.partial_directed_coherence),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 3 PDC',
                                     font_size=8)

dtf_fig = sails.plotting.plot_vector(np.abs(F.directed_transfer_function),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 3 DTF',
                                     font_size=8)

pdc_fig.savefig('gallery_bac2001_fig3_pdc.png', dpi=150)
dtf_fig.savefig('gallery_bac2001_fig3_dtf.png', dpi=150)

plt.close('all')

# Baccala 2001 Figure 4
sample_rate = 128

sig_bac2001_fig4 = sails.simulate.Baccala2001_fig4()
m = sig_bac2001_fig4.generate_model()
freq_vect = np.linspace(0, sample_rate / 2, 128)
F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)

pdc_fig = sails.plotting.plot_vector(np.abs(F.partial_directed_coherence),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 4 PDC',
                                     font_size=8)

dtf_fig = sails.plotting.plot_vector(np.abs(F.directed_transfer_function),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 4 DTF',
                                     font_size=8)

pdc_fig.savefig('gallery_bac2001_fig4_pdc.png', dpi=150)
dtf_fig.savefig('gallery_bac2001_fig4_dtf.png', dpi=150)

plt.close('all')


# Baccala 2001 Figure 5
sample_rate = 128

sig_bac2001_fig5 = sails.simulate.Baccala2001_fig5()
m = sig_bac2001_fig5.generate_model()
freq_vect = np.linspace(0, sample_rate / 2, 128)
F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)

pdc_fig = sails.plotting.plot_vector(np.abs(F.partial_directed_coherence),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 5 PDC',
                                     font_size=8)
dtf_fig = sails.plotting.plot_vector(np.abs(F.directed_transfer_function),
                                     freq_vect / sample_rate,
                                     diag=True,
                                     title='Baccala 2001 Fig 5 DTF',
                                     font_size=8)

pdc_fig.savefig('gallery_bac2001_fig5_pdc.png', dpi=150)
dtf_fig.savefig('gallery_bac2001_fig5_dtf.png', dpi=150)

plt.close('all')

# Pascual-Marqui et-al 2014
sample_rate = 256

sig_pm2014_fig3 = sails.simulate.PascualMarqui2014_fig3()
m = sig_pm2014_fig3.generate_model()
freq_vect = np.linspace(0, sample_rate / 2, 128)
F = sails.FourierMvarMetrics.initialise(m, sample_rate, freq_vect)

iec_fig = sails.plotting.plot_vector(np.abs(F.isolated_effective_coherence),
                                     freq_vect,
                                     diag=False,
                                     title='Pascual-Marqui et al 2014 Figure 3 IEC',
                                     font_size=8)

pdc_fig = sails.plotting.plot_vector(np.abs(F.partial_directed_coherence),
                                     freq_vect,
                                     diag=False,
                                     title='Pascual-Marqui et al 2014 Figure 3 PDC',
                                     font_size=8)

iec_fig.savefig('gallery_pm2014_fig3_iec.png', dpi=150)
pdc_fig.savefig('gallery_pm2014_fig3_pdc.png', dpi=150)

plt.close('all')
