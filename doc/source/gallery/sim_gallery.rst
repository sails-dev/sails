Simulation Gallery
==================

This page contains a set of figures re-created around existing papers which
used simulated or analytic systems.  These graphs are provided for comparison
with the original papers.

The code for generating these figures can be found in :download:`sim_gallery.py`.

Baccala and Sameshima 2001
--------------------------

We re-create figures 1 to 5 of this paper.  Each figure compares
PDC and DTF connectivity estimators.

.. image:: gallery_bac2001_fig1_pdc.png

.. image:: gallery_bac2001_fig1_dtf.png

.. image:: gallery_bac2001_fig2_pdc.png

.. image:: gallery_bac2001_fig2_dtf.png

.. image:: gallery_bac2001_fig3_pdc.png

.. image:: gallery_bac2001_fig3_dtf.png

.. image:: gallery_bac2001_fig4_pdc.png

.. image:: gallery_bac2001_fig4_dtf.png

.. image:: gallery_bac2001_fig5_pdc.png

.. image:: gallery_bac2001_fig5_dtf.png


Pascual-Marqui et al 2014
-------------------------

We re-create figure 3 of this paper using IEC and PDC.
PDC and DTF connectivity estimators.

.. image:: gallery_pm2014_fig3_iec.png

.. image:: gallery_pm2014_fig3_pdc.png



