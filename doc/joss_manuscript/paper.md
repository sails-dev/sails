---
title: 'SAILS: Spectral Analysis In Linear Systems'
tags:
  - Python
  - Spectrum
  - Dynamics
  - Connectivity
  - Modal
authors:
  - name: Andrew J. Quinn
    orcid: 0000-0003-2267-9897
    affiliation: 1
  - name: Mark Hymers
    affiliation: 2
affiliations:
 - name: Oxford Centre for Human Brain Activity, Wellcome Centre for Integrative Neuroimaging, Department of Psychiatry, University of Oxford, UK.
   index: 1
 - name: York NeuroImaging Centre, Department of Psychology, University of York, UK.
   index: 2
date: 21 November 2019
bibliography: paper.bib
---

# Summary & Background

Autoregressive modelling provides a powerful and flexible parametric approach
to modelling uni- or multi-variate time-series data.  AR models have
mathematical links to linear time-invariant systems, digital filters and Fourier
based frequency analyses.  As such, a wide range of time-domain and
frequency-domain metrics can be readily derived from the fitted autoregressive
parameters. These approaches are fundamental in a wide range of science and
engineering fields and still undergoing active development.  ``SAILS``
(Spectral Analysis in Linear Systems) is a python package which
implements such methods and provides a basis for both the straightforward
fitting of AR models as well as exploration and development of newer methods,
such as the decomposition of autoregressive parameters into eigenmodes.

# Package Features

The SAILS toolbox is designed to work with time-series data from any form of
application, but was originally written by the authors with an intended use in
human neuroscience.   The package provides functionality for model fitting,
(for example Ordinary Least-Squares and Vieira-Morf [@Marple1987] approaches),
model selection (Akaike's Information Criterion and Bayesian Information
Criterion), model validation (e.g. Stability Index [@Lutkepohl2007],
Durbin-Watson criteria [@DurbinWatson1950] and Percent Consistency [@Ding2000])
Once fitted, a range of spectral features such as the power spectral density
and the transfer function can be estimated from the fitted model parameters. An
additional range of connectivity metrics can be computed from multivariate
models - including Magnitude Squared Coherence, Partial Coherence, Directed
Transfer Function [@kaminski_new_1991], Partial Directed Coherence
[@baccala_partial_2001] and Isolated Effective Coherence
[@pascual-marqui_assessing_2014]. SAILS is written in a modular form and
designed to be easily extensible. The authors intend to continue expanding the
range of model fitting and connectivity methods as they are developed.

Advanced exploration of the spectral content of the model is provided via a
modal decomposition of the fitted autoregressive parameters [@Neumaier2001], an
alternative to analyses which require the use of Fourier transform. This method
can provide an intuitive summary of the frequency content of the system via a
set of oscillatory modes - each defined by a peak resonant frequency, a damping
time and a projection into the full network. The transfer function of the
system can be computed by linear summation of these modal parameters.

The software also includes plotting routines to make examining data easier for
the user.  These range from methods to assist in plotting matrices of frequency
information across multiple nodes to routines to simplify the use of external
plotting programs such as Circos [@Krzywinski2009] for circular plots.
Examples of such plots are included in the Screenshots section of this paper.

# Target Audience

SAILS is aimed for use in scientific and engineering data analysis of
multivariate oscillating systems. As this opens up a wide range of potential
use cases, we include a incomplete set of examples for illustration. An
economist might be interested to use power spectra to quantify oscillatory or
cyclical changes in economic variables such as financial markets. A biomedical
engineer analysing M/EEG data may be interested in the spectral connectivity
metrics for quantifying how oscillatory synchrony in brain networks changes
between clinical populations. Similarly, a cognitive neuroscientist could use
the sliding window model fits on MEG data to explore how brain networks change
during a cognitive task. As a final example, an engineer or physicist may use
the functions to explore resonant vibrations in a physical system. In practice,
these features can be used in on any time-series to quantify oscillatory
features, their spatial distribution and network interactions.

# State of the field

Several Python packages provide related functionality to SAILS. Firstly, there
are a range of packages implementing multivariate regression fits (including
[numpy.linalg](https://docs.scipy.org/doc/numpy/reference/routines.linalg.html#solving-equations-and-inverting-matrices)
and [statsmodels.tsa](https://www.statsmodels.org/stable/tsa.html)). SAILS is
readily extensible to work with model fits from other packages by creating a
class inheriting from AbstractLinearModel and implementing the fit method to
call the external package. Some spectral connectivity measures (based on
multi-taper or Wavelet transforms and not currently including MVAR based
measures such as Partial Directed Coherence) are implemented in
[MNE-python](https://mne.tools/stable/generated/mne.connectivity.spectral_connectivity.html),
a specialised electrophysiology analysis package. Other packages (such as
[pyspectrum](https://pyspectrum.readthedocs.io)) provide autoregressive
spectrum estimation methods but are not readily extensible to multivariate
datasets or connectivity metrics. Finally, some packages implement varying
range of autoregressive connectivity estimates (such as
[nitime](http://nipy.org/nitime/), [SCoT](https://github.com/scot-dev/scot),
[connectivipy](connectivipy.readthedocs.io) and
[spectral_connectivity](https://github.com/Eden-Kramer-Lab/spectral_connectivity)).
SAILS provides overlapping functionality to these packages alongside advanced
methods such as the modal decomposition.

# Availability and Installation

SAILS is released under a GPLv2 or later license.

The toolbox is available on [PyPi](https://pypi.org) and can therefore be installed
using pip using ```pip install sails``` or similar.  It has minimal dependencies
although if users wish to produce circular plots, [circos](https://circos.ca)
will need to be installed either from the website or via the users package
manager on systems such as Debian or Ubuntu.

The package is designed for use under Python 3.  Most functions will work under
Python 2, but the authors no longer actively test under this version.

# Implementation and Usage

Tutorials and documentation regarding the use of the module are available at
https://sails.readthedocs.org.  Development and bug tracking is
hosted at https://github.com/sails-dev/sails.

The majority of workflows can be summarised as:

 1. Determine the appropriate model order for the time-series (Tutorial example: Exploring model order)
 2. Fit a model to the data (Tutorial example: Fitting real univariate data)
 3. Examine metrics on the model (Tutorial example: MVAR connectivity estimation)
 4. Plot results (Tutorial example: Plotting helpers)

The toolbox includes a test suite based around the python py.test module.
This test suite runs from the continuous integration environment on the
gitlab server and the authors intend to continue expanding the coverage
of the test suite.

# Screenshots

All screenshots are based around material found in the online tutorials
(https://sails.readthedocs.org) in which code to generate them can be found.

![Example of plot using AIC to determine model order](aic.png)

![Example of a Partial Directed Coherence matrix plot](F_PDC.png)

![Example of plot showing the pole representation of the AR system](poleplot.png)

![Example of circular connectivity plot generated using Circos](aal_circular.png)

![Example of netmat connectivity plot](aal_netmat.png)

# References
