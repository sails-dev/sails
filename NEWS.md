# 1.1.1
 * Fix sklearn version in requirements.txt

# 1.1.0

 * Add ability to use some sklearn estimators when fitting models
 * Add data epoching functions
 * Add functions for handling reduced fits using PCA
 * Add Morlet waveleft functions
 * Add optional anamnesis support
 * Add single pole oscillator simulation functionality
 * Improve artefact detection routines
 * Improve colour options in circular plots
 * Speed up `ar_spectrum` routine by ~10x

# 1.0.0

 * Initial release
